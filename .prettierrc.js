module.exports = {
  bracketSpacing: true,
  jsxBracketSameLine: true,
  singleQuote: true,
  trailingComma: 'all',
  semi: true,
  endOfLine: 'auto',
  tabWith: 2,
  code: 80,
  useTabs: false,
  arrowParens: 'avoid',
};
