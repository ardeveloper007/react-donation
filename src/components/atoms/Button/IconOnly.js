import React from 'react';
import { View } from 'react-native';
import { IcCekout, IcAdd } from '../../../assets';

const IconOnly = ({ icon }) => {
  const Icon = () => {
    if (icon === 'cekout') {
      return <IcCekout />;
    }
    if (icon === 'add') {
      return <IcAdd />;
    }
    return <IcAdd />;
  };
  return (
    <View>
      <Icon />
    </View>
  );
};
export default IconOnly;
