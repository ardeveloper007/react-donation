import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { colors } from '../../../utils';
import IconOnly from './IconOnly';

const Button = ({ type, tittle, onPress, icon, btn, disable }) => {
  if (icon === 'cekout' || icon === 'add') {
    return (
      <>
        <TouchableOpacity
          disabled={disable ? true : false}
          activeOpacity={0.7}
          style={styles.fab(icon, disable)}
          onPress={onPress}>
          <IconOnly icon={icon} />
        </TouchableOpacity>
      </>
    );
  }
  if (btn) {
    return (
      <TouchableOpacity
        activeOpacity={0.7}
        style={styles.btnStyle}
        onPress={onPress}>
        <Text style={styles.btn}>{tittle}</Text>
      </TouchableOpacity>
    );
  }
  if (disable) {
    return (
      <View style={styles.containerDisable}>
        <Text style={styles.textDisable}>{tittle}</Text>
      </View>
    );
  }
  if (type === 'cancel') {
    return (
      <TouchableOpacity
        activeOpacity={0.5}
        style={styles.containerCancel}
        onPress={onPress}>
        <Text style={styles.text(type)}>{tittle}</Text>
      </TouchableOpacity>
    );
  }
  return (
    <TouchableOpacity
      activeOpacity={0.5}
      style={styles.container(type)}
      onPress={onPress}>
      <Text style={styles.text(type)}>{tittle}</Text>
    </TouchableOpacity>
  );
};
export default Button;

const styles = StyleSheet.create({
  textDisable : {
    fontSize: 14,
    fontFamily: 'Poppins-Medium',
    textAlign: 'center',
    color: colors.eighth,
  },
  btnStyle: {
    marginVertical: 25,
  },
  btn: {
    fontSize: 16,
    fontFamily: 'Poppins-Medium',
    color: colors.primary,
  },
  fab: (type, disable) => ({
    alignItems: 'center',
    position: 'absolute',
    bottom: type === 'cekout' ? 10 : 80,
    right: type === 'cekout' ? 10 : 20,
    backgroundColor: disable === 'disable' ? colors.fourth : colors.primary,
    padding: type === 'cekout' ? 15 : 10,
    borderRadius: 999,
    zIndex: 10,
  }),
  containerDisable: {
    backgroundColor: colors.fourth,
    paddingVertical: 10,
    borderRadius: 10,
    flex: 1,
  },
  containerCancel: {
    backgroundColor: colors.fifth,
    paddingVertical: 10,
    borderRadius: 10,
    flex: 1,
  },
  container: type => ({
    backgroundColor: type === 'secondary' ? colors.secondary : colors.primary,
    paddingVertical: 10,
    borderRadius: 10,
    flex: 1,
    width: '100%',
    alignContent: 'center',
    justifyContent: 'center',
    alignItems: 'center'
  }),
  text: type => ({
    fontSize: 14,
    fontFamily: 'Poppins-Medium',
    textAlign: 'center',
    color: type === 'secondary' ? colors.primary : colors.secondary,
  }),
});
