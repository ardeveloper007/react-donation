const mainColors = {
  blue: '#34407D',
  yellow: '#FBFF22',
  white: '#FFFFFF',
  gray: '#E5E5E5',
  red: '#DF2020',
  blueLink: '#2300FC',
  whiteFlight : '#F2F2F2',
  manatee : '#8D92A3'
};

export const colors = {
  primary: mainColors.blue,
  secondary: mainColors.yellow,
  tertiary: mainColors.white,
  fourth: mainColors.gray,
  fifth: mainColors.red,
  sixth: mainColors.blueLink,
  seventh: mainColors.whiteFlight,
  eighth :  mainColors.manatee
};
