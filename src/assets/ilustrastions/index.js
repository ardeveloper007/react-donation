import ILLBocil from './bocil.png';
import ILLCor1 from './corousel(1).png';
import ILLCor2 from './corousel(2).png';
import ILLCor3 from './corousel(3).png';

export { ILLBocil, ILLCor1, ILLCor2, ILLCor3 };
