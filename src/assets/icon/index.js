import IcRemoveImageProfile from './ic-remove-photo-profil.svg';
import IcAddImageProfil from './ic-add-photo-profil.svg';
import IcHistoryActive from './ic-history-active.svg';
import IcProfilDefault from './ic-profil-default.svg';
import IcRemoveImage from './ic-remove-photo.svg'
import IcLogoMknKita from './ic-logo-mknKita.png';
import ICCloseModals from './ic-modal-close.svg';
import IcHomeActive from './ic-home-active.svg';
import IcUserActive from './ic-user-active.svg';
import IcAddImage from './ic-add-image.svg';
import IcCalender from './ic-calendar.svg';
import IcHistory from './ic-history.svg';
import IcBtnAdd from './ic-btn-add.svg';
import IcDetail from './ic-detail.svg';
import IcCekout from './ic-cekout.svg';
import IcDelete from './ic-delete.svg';
import IcBack from './ic-back.svg';
import IcLogo from './ic-logo.png';
import IcHome from './ic-home.svg';
import IcUser from './ic-user.svg';
import IcNext from './ic-next.svg';
import IcAdd from './ic-add.svg';

export {
  IcRemoveImageProfile,
  IcAddImageProfil,
  IcHistoryActive,
  IcProfilDefault,
  IcRemoveImage,
  IcLogoMknKita,
  ICCloseModals,
  IcHomeActive,
  IcUserActive,
  IcAddImage,
  IcCalender,
  IcHistory,
  IcDetail,
  IcDelete,
  IcBtnAdd,
  IcCekout,
  IcNext,
  IcAdd,
  IcBack,
  IcLogo,
  IcHome,
  IcUser,
};
