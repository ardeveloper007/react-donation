const initProfile = {
  user: {},
  member: {},
};

const initStateRegister = {
  email: '',
  password: '',
};

export const registerReducer = (state = initStateRegister, action) => {
  if (action.type === 'SET_REGISTER') {
    return {
      ...state,
      email: action.value.email,
      password: action.value.password,
    };
  }
  return state;
};

export const fetchProfileReducer = (state = initProfile, action) => {
  if (action.type === 'FETCH_PROFILE') {
    return {
      ...state,
      user: action.value.user,
      member: action.value.member,
    };
  }
  return state;
};

const initPhoto = {
  uri: '',
  type: '',
  name: '',
  isUploadPhoto: false,
};

export const photoReducer = (state = initPhoto, action) => {
  if (action.type === 'SET_PHOTO') {
    return {
      ...state,
      uri: action.value.uri,
      type: action.value.type,
      name: action.value.name,
    };
  }

  if (action.type === 'SET_UPLOAD_STATUS') {
    return {
      ...state,
      isUploadPhoto: action.value,
    };
  }
  return state;
};
