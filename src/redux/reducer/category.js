const initCategoryState = {
  category: [],
};

export const categoryReducer = (state = initCategoryState, action) => {
  if (action.type === 'SET_CATEGORY') {
    return {
      ...state,
      category: action.value,
    };
  }

  return state;
};
