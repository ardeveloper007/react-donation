import Axios from 'axios';
import { API_HOST } from '../../config';
import { getData, showMessage } from '../../utils';
import { setLoading } from './global';

export const getItem = () => dispatch => {
  dispatch(setLoading(true));
  getData('token').then(resToken => {
    Axios.get(`${API_HOST.url}/get-cart-item`, {
      headers: {
        Authorization: resToken.value,
      },
    })
      .then(res => {
        dispatch(setLoading(false));
        dispatch({ type: 'SET_ITEM', value: res.data.data });
      })
      .catch(err => {
        showMessage(err?.response?.data?.data?.message);
      });
  });
};
