import AsyncStorage from '@react-native-async-storage/async-storage';
import Axios from 'axios';
import { API_HOST } from '../../config';
import { getData, showMessage, storeData } from '../../utils';
import { setLoading } from './global';

export const signInAction = (form, navigation) => dispatch => {
  dispatch(setLoading(true));
  Axios.post(`${API_HOST.url}/login`, form)
    .then(res => {
      const token = `${res.data.data.token_type} ${res.data.data.access_token}`;
      const profile = {
        ...res.data.data.user,
        ...res.data.data.member,
      };
      dispatch(setLoading(false));
      storeData('token', {
        value: token,
      });
      profile.foto = `${API_HOST.storage}photo-profile/${res.data.data.member.foto}`;
      storeData('userProfile', profile);
      navigation.reset({ index: 0, routes: [{ name: 'MainApp' }] });
    })
    .catch(err => {
      dispatch(setLoading(false));
      showMessage(err?.response?.data?.data?.message);
    });
};

export const signUpAction = (form, photoReducer, navigation) => dispatch => {
  dispatch(setLoading(true));
  Axios.post(`${API_HOST.url}/register`, form)
    .then(res => {
      const token = `${res.data.data.token_type} ${res.data.data.access_token}`;
      const profile = {
        ...res.data.data.user,
        ...res.data.data.member,
      };
      storeData('token', {
        value: token,
      });
      if (photoReducer.isUploadPhoto) {
        const photoForUpload = new FormData();
        photoForUpload.append('foto', photoReducer);
        Axios.post(`${API_HOST.url}/update-photo`, photoForUpload, {
          headers: {
            Authorization: token,
            'Content-Type': 'multipart/form-data',
          },
        })
          .then(resUpload => {
            profile.foto = `${API_HOST.storage}photo-profile/${resUpload.data.data[0]}`;
            storeData('userProfile', profile);
            navigation.reset({ index: 0, routes: [{ name: 'MainApp' }] });
          })
          .catch(errUpload => {
            showMessage(
              errUpload?.response?.message || 'Upload photo tidak berhasil',
            );
            navigation.reset({ index: 0, routes: [{ name: 'MainApp' }] });
          });
      } else {
        storeData('userProfile', profile);
        navigation.reset({ index: 0, routes: [{ name: 'MainApp' }] });
      }

      dispatch(setLoading(false));
    })
    .catch(err => {
      dispatch(setLoading(false));
      showMessage(
        err?.response?.data?.data?.message || 'Register tidak berhasil',
      );
    });
};

export const signOutAction = navigation => dispatch => {
  dispatch(setLoading(true));
  getData('token').then(resToken => {
    Axios.post(`${API_HOST.url}/logout`, null, {
      headers: {
        Authorization: resToken.value,
      },
    }).then(() => {
      dispatch(setLoading(false));
      AsyncStorage.multiRemove(['userProfile', 'token']).then(() => {
        navigation.reset({ index: 0, routes: [{ name: 'GetStarted' }] });
      });
    });
  });
};

export const fetchProfile = () => dispatch => {
  dispatch(setLoading(true));
  getData('token').then(resToken => {
    Axios.get(`${API_HOST.url}/fetch-profile`, {
      headers: {
        Authorization: resToken.value,
      },
    })
      .then(res => {
        dispatch(setLoading(false));
        const user = res.data.data.user;
        const member = res.data.data.member;
        dispatch({ type: 'FETCH_PROFILE', value: [...user, ...member] });
      })
      .catch(err => {
        showMessage(err?.response?.data?.data?.message);
      });
  });
};

export const updateProfileAction =
  (form, photoReducer, navigation) => dispatch => {
    console.log(form);
    console.log(photoReducer);
    dispatch(setLoading(true));
    getData('token').then(resToken => {
      Axios.post(`${API_HOST.url}/update-profile`, form, {
        headers: {
          Authorization: resToken.value,
        },
      })
        .then(res => {
          const profile = {
            ...res.data.data.user,
            ...res.data.data.member,
          };
          if (photoReducer.isUploadPhoto) {
            const photoForUpload = new FormData();
            photoForUpload.append('foto', photoReducer);
            Axios.post(`${API_HOST.url}/update-photo`, photoForUpload, {
              headers: {
                Authorization: resToken.value,
                'Content-Type': 'multipart/form-data',
              },
            })
              .then(resUpload => {
                dispatch(setLoading(false));
                profile.foto = `${API_HOST.storage}photo-profile/${resUpload.data.data[0]}`;
                storeData('userProfile', profile);
                navigation.reset({
                  index: 0,
                  routes: [{ name: 'MainApp', screen: 'Profile' }],
                });
              })
              .catch(errUpload => {
                dispatch(setLoading(false));
                showMessage(
                  errUpload?.response?.message || 'Upload photo tidak berhasil',
                );
              });
          } else {
            dispatch(setLoading(false));
            profile.foto = `${API_HOST.storage}photo-profile/${res.data.data.member.foto}`;
            storeData('userProfile', profile);
            navigation.reset({
              index: 0,
              routes: [{ name: 'MainApp', screen: 'Profile' }],
            });
          }
        })
        .catch(err => {
          dispatch(setLoading(false));
          showMessage(
            err?.response?.data?.data?.message || 'Update tidak berhasil',
          );
        });
    });
  };
