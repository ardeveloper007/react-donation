import React, { useState, useEffect } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Modal,
  Pressable,
  ScrollView,
  useWindowDimensions,
} from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { IcDelete } from '../../assets';
import { Button, Gap, Header, TextInput } from '../../components';
import { signInAction, fetchTerms } from '../../redux/action';
import { colors, useForm } from '../../utils';
import RenderHtml from 'react-native-render-html';

const Login = ({ navigation }) => {
  const [form, setForm] = useForm({
    email: '',
    password: '',
  });

  const { width } = useWindowDimensions();
  const { terms } = useSelector(state => state.termsReducer);

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchTerms());
  }, [dispatch]);

  const onSubmit = () => {
    dispatch(signInAction(form, navigation));
  };

  const [modalVisible, setModalVisible] = useState(false);

  const no = () => {
    setModalVisible(!modalVisible);
  };
  return (
    <>
      <Modal animationType="none" transparent={true} visible={modalVisible}>
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <ScrollView>
              <Text style={styles.modalText}>Syarat dan Ketentuan</Text>
              {terms === null ? (
                <View>
                  <Text />
                </View>
              ) : (
                <RenderHtml
                  contentWidth={width}
                  source={{
                    html: `<style>p{font-family: 'Poppins-Light'; font-size: 16px;}</style><div style="text-align: center; margin-bottom: 15px;">${terms.isi}</div>`,
                  }}
                />
              )}
            </ScrollView>
            <View style={styles.btnStyle}>
              <Pressable style={styles.button} onPress={() => no()}>
                <View style={styles.btnClose}>
                  <IcDelete />
                </View>
              </Pressable>
            </View>
          </View>
        </View>
      </Modal>
      <Header title="Selamat Datang" deskripsi="Silahkan Masuk" />
      <View style={styles.container}>
        <View>
          <View>
            <TextInput
              label="Email"
              placeholder="Masukkan Email"
              value={form.email}
              onChangeText={value => setForm('email', value)}
            />
            <Gap height={20} />
            <TextInput
              label="Password"
              placeholder="Masukkan Password"
              value={form.password}
              onChangeText={value => setForm('password', value)}
              secureTextEntry
            />
          </View>
          <Gap height={40} />
          <View style={styles.btnForm}>
            {form.email !== '' && form.password !== '' ? (
              <Button tittle="Masuk" onPress={onSubmit} />
            ) : (
              <Button tittle="Masuk" type="disable" disable />
            )}
          </View>
        </View>

        <View>
          <Text style={styles.text}>Belum punya akun?</Text>
          <Gap height={12} />
          <View style={styles.btnForm}>
            <Button
              type="secondary"
              tittle="Buat akun"
              onPress={() => navigation.navigate('Register')}
            />
          </View>
          <Gap height={12} />
          <Pressable onPress={() => setModalVisible(true)}>
            <Text style={styles.text}>Syarat dan ketentuan</Text>
          </Pressable>
          <Gap height={50} />
        </View>
      </View>
    </>
  );
};
export default Login;

const styles = StyleSheet.create({
  btnStyle: {
    top: 20,
    position: 'absolute',
    right: 20,
  },
  btnClose: {
    padding: 10,
  },
  textStyle: {
    color: colors.secondary,
    fontFamily: 'Poppins-Light',
    fontSize: 12,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  button: {
    height: 50,
  },
  btnForm: {
    height: 50,
  },
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    height: '100%',
    backgroundColor: 'rgba(52,64,125,0.5)',
  },
  modalView: {
    marginHorizontal: 10,
    marginVertical: 10,
    backgroundColor: 'white',
    borderRadius: 10,
    padding: 35,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
    borderColor: colors.primary,
    borderWidth: 1,
  },
  modalText: {
    marginTop: 30,
    marginBottom: 15,
    textAlign: 'center',
    fontFamily: 'Poppins-Light',
    color: colors.primary,
    fontSize: 16,
  },
  text: {
    textAlign: 'center',
    fontFamily: 'Poppins-Medium',
    color: colors.primary,
    fontSize: 10,
  },
  container: {
    backgroundColor: colors.tertiary,
    justifyContent: 'space-between',
    flex: 1,
    paddingHorizontal: 25,
  },
});
