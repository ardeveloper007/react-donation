import Axios from 'axios';
import React, { useEffect, useState } from 'react';
import {
  Image,
  PermissionsAndroid,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import { launchImageLibrary } from 'react-native-image-picker';
import { useSelector, useDispatch } from 'react-redux';
import { IcAddImage, IcRemoveImage } from '../../assets';
import { Button, Gap, Header, TextInput } from '../../components';
import { API_HOST } from '../../config';
import { fetchCategory, fetchQuantity, setLoading } from '../../redux/action';
import { colors, getData, showMessage, useForm } from '../../utils';

const AddDonasi = ({ navigation }) => {
  const [hasPhoto, setHasPhoto] = useState(false);
  const [form, setForm] = useForm({
    nama_barang: '',
    donation_categories_id: '',
    jumlah: '',
    quantities_id: '',
    foto: '',
  });

  const [photo, setPhoto] = useState('');
  const { quantityReducer, categoryReducer } = useSelector(state => state);

  const dispatch = useDispatch();

  useEffect(() => {
    navigation.addListener('focus', () => {
      dispatch(fetchQuantity());
      dispatch(fetchCategory());
    });
  }, [dispatch, navigation]);

  const getImage = async () => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        {
          title: 'Allow to access photos',
          buttonNeutral: 'Ask Me Later',
          buttonNegative: 'Cancel',
          buttonPositive: 'OK',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log('camera di izinkan');
        launchImageLibrary(
          { includeBase64: true, maxWidth: 200, maxHeight: 200 },
          response => {
            if (response.didCancel || response.errorCode) {
              showMessage('Anda tidak memilih photo');
            } else {
              const source = { uri: response.assets[0].uri };
              const dataImage = {
                uri: response.assets[0].uri,
                type: response.assets[0].type,
                name: response.assets[0].fileName,
              };
              setForm('foto', dataImage);
              setPhoto(source);
              setHasPhoto(true);
            }
          },
        );
      } else {
        showMessage('Tidak di izinkan');
      }
    } catch (err) {
      showMessage(err);
    }
  };

  const submit = () => {
    dispatch(setLoading(true));
    getData('token').then(resToken => {
      const formItem = new FormData();
      formItem.append('donation_categories_id', form.donation_categories_id);
      formItem.append('quantities_id', form.quantities_id);
      formItem.append('nama_barang', form.nama_barang);
      formItem.append('jumlah', form.jumlah);
      formItem.append('foto', form.foto);
      Axios.post(`${API_HOST.url}/store-item`, formItem, {
        headers: {
          Authorization: resToken.value,
          'Content-Type': 'multipart/form-data',
        },
      })
        .then(res => {
          dispatch(setLoading(false));
          navigation.reset({ index: 0, routes: [{ name: 'MainApp' }] });
        })
        .catch(error => {
          dispatch(setLoading(false));
          showMessage(error?.response?.message || 'Tambah item tidak berhasil');
        });
    });
  };

  return (
    <View style={styles.page}>
      <Header single title="Tambah Donasi" onBack={() => navigation.goBack()} />
      <ScrollView showsVerticalScrollIndicator={false} style={styles.container}>
        <TextInput
          label="Nama Barang"
          placeholder="Masukkan Nama Barang"
          value={form.nama_barang}
          onChangeText={value => setForm('nama_barang', value)}
        />
        <Gap height={12} />
        <TextInput
          label="Jenis Barang"
          placeholder="Pilih jenis barang"
          selectProvinsi
          selectItem={categoryReducer.category}
          value={form.donation_categories_id}
          onValueChange={value => setForm('donation_categories_id', value)}
        />
        <Gap height={12} />
        <View style={styles.kuantitas}>
          <View style={styles.form}>
            <TextInput
              label="Jumlah"
              placeholder="Jumlah barang"
              keyboardType="numeric"
              value={form.jumlah}
              onChangeText={value => setForm('jumlah', value)}
            />
          </View>
          <Gap width={8} />
          <View style={styles.form}>
            <Gap height={7} />
            <TextInput
              placeholder="Pilih Kuantitas"
              selectProvinsi
              selectItem={quantityReducer.quantity}
              value={form.quantities_id}
              onValueChange={value => setForm('quantities_id', value)}
            />
          </View>
        </View>
        <Gap height={12} />
        <Text style={styles.label}>Masukkan foto </Text>
        <View>
          <TouchableOpacity onPress={getImage}>
            <Image source={photo} style={styles.conImage} />
            {hasPhoto && <IcRemoveImage style={styles.icon} />}
            {!hasPhoto && <IcAddImage style={styles.icon} />}
          </TouchableOpacity>
        </View>
        <Gap height={64} />
        {form.nama_barang !== '' &&
        form.donation_categories_id !== '' &&
        form.jumlah !== '' &&
        form.quantities_id !== '' &&
        form.foto !== '' ? (
          <View style={styles.button}>
            <Button tittle="Tambah" onPress={submit} />
          </View>
        ) : (
          <Button tittle="Tambah" type="disable" disable />
        )}
      </ScrollView>
    </View>
  );
};
export default AddDonasi;

const styles = StyleSheet.create({
  icon: {
    justifyContent: 'center',
    alignItems: 'center',
    alignContent: 'center',
    flex: 1,
    position: 'absolute',
    right: 200,
    top: 50,
  },
  conImage: {
    width: 260,
    height: 160,
    borderWidth: 1,
    borderColor: colors.primary,
    borderRadius: 8,
    padding: 10,
  },
  label: {
    fontSize: 16,
    fontFamily: 'Poppins-Regular',
    color: colors.primary,
  },
  form: {
    flex: 1,
  },
  kuantitas: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  container: {
    paddingHorizontal: 24,
  },
  page: {
    flex: 1,
    backgroundColor: colors.tertiary,
  },
});
