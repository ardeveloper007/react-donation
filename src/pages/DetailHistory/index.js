import Axios from 'axios';
import moment from 'moment';
import React, { useEffect, useState } from 'react';
import {
  ScrollView,
  StyleSheet,
  View,
  Modal,
  Text,
  Pressable,
} from 'react-native';
import { useDispatch } from 'react-redux';
import { Gap, Header, RowText, Button } from '../../components';
import { API_HOST } from '../../config';
import { setLoading } from '../../redux/action';
import { colors, getData, showMessage } from '../../utils';

const DetailHistory = ({ navigation, route }) => {
  const donationDetail = route.params;
  const [donation, setDonation] = useState({});
  const [donationItem, setDonationItem] = useState([]);
  const [donationHistory, setDonationHistory] = useState([]);
  const [modalVisible, setModalVisible] = useState(false);

  const dispatch = useDispatch();

  useEffect(() => {
    navigation.addListener('focus', () => {
      dispatch(setLoading(true));
      getData('token').then(resToken => {
        Axios.get(`${API_HOST.url}/donation-details/${donationDetail}`, {
          headers: {
            Authorization: resToken.value,
          },
        })
          .then(res => {
            dispatch(setLoading(false));
            setDonation(res.data.data);
            setDonationItem(res.data.data.items);
            setDonationHistory(res.data.data.histories);
          })
          .catch(err => {
            showMessage(err?.response?.message || 'Gagal mengambil data');
          });
      });
    });
  }, [
    navigation,
    donation,
    donationDetail,
    donationHistory,
    donationItem,
    dispatch,
  ]);

  const yes = () => {
    setModalVisible(!modalVisible);
    var form = {
      id: donation.id,
    };
    dispatch(setLoading(true));
    getData('token').then(resToken => {
      Axios.post(`${API_HOST.url}/cancel-donation`, form, {
        headers: {
          Authorization: resToken.value,
        },
      })
        .then(res => {
          dispatch(setLoading(false));
          navigation.reset({ index: 0, routes: [{ name: 'MainApp' }] });
        })
        .catch(error => {
          dispatch(setLoading(false));
          showMessage(
            error?.response?.message || 'Batalkan donasi tidak berhasil',
          );
        });
    });
  };
  const no = () => {
    setModalVisible(!modalVisible);
  };

  return (
    <View style={styles.page}>
      <Header
        title="Detail Riwayat"
        single
        onBack={() => navigation.goBack()}
      />

      <Modal animationType="none" transparent={true} visible={modalVisible}>
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <Text style={styles.modalText}>
              Apakah anda yakin untuk membatalkan donasi?
            </Text>
            <View style={styles.btnStyle}>
              <View>
                <Pressable style={styles.buttonModal} onPress={yes}>
                  <Text style={styles.textStyle}>Ya</Text>
                </Pressable>
              </View>
              <View>
                <Gap width={14} />
              </View>
              <View>
                <Pressable style={styles.buttonModal} onPress={no}>
                  <Text style={styles.textStyle}>Tidak</Text>
                </Pressable>
              </View>
            </View>
          </View>
        </View>
      </Modal>

      <ScrollView showsVerticalScrollIndicator={false} style={styles.container}>
        <RowText title="Nama barang" />
        {donationItem.map(item => {
          return (
            <RowText
              key={item.id}
              deskripsi={item.nama_barang}
              value={`${item.jumlah} ${item.quantity}`}
            />
          );
        })}
        <RowText title="Tanggal" />
        <RowText
          deskripsi="Tanggal donasi"
          value={`${moment(donation.donation_date).format('D MMMM YYYY')}`}
        />
        <RowText
          deskripsi="Tanggal pengambilan"
          value={`${moment(donation.pick_up_date).format('D MMMM YYYY')}`}
        />

        <RowText title="Donatur" />
        <RowText deskripsi="Nama" value={donation.members} />
        <RowText deskripsi="No. hp" value={donation.no_telp} />
        <RowText deskripsi="Alamat" value={donation.alamat} />

        <RowText deskripsi="Status Donasi" value={donation.status} />
        <View>
          <RowText title="Riwayat" />
          <RowText
            deskripsi="Detail"
            value="Lihat detail"
            link
            onPress={() => navigation.navigate('DetailTransaksi', donation)}
          />
        </View>
        {donation.status === 'Menunggu' ? (
          <>
            <View>
              <Gap height={60} />
              <View style={styles.button}>
                <Button
                  tittle="Batalkan Donasi"
                  type="cancel"
                  onPress={() => setModalVisible(true)}
                />
              </View>
            </View>
          </>
        ) : (
          <View />
        )}

        <Gap height={50} />
      </ScrollView>
    </View>
  );
};
export default DetailHistory;

const styles = StyleSheet.create({
  buttonModal: {
    borderRadius: 10,
    paddingVertical: 12,
    elevation: 2,
    backgroundColor: colors.primary,
    width: 117,
    height: 42,
  },
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    height: '100%',
    backgroundColor: 'rgba(52,64,125,0.5)',
  },
  modalView: {
    margin: 20,
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 35,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
    borderColor: colors.primary,
    borderWidth: 1,
  },
  modalText: {
    marginBottom: 15,
    textAlign: 'center',
    fontFamily: 'Poppins-Light',
    color: colors.primary,
    fontSize: 16,
  },
  textStyle: {
    color: colors.secondary,
    fontFamily: 'Poppins-Light',
    fontSize: 12,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  btnStyle: {
    flexDirection: 'row',
    alignContent: 'space-between',
  },
  center: {
    textAlign: 'center',
    borderWidth: 1,
    paddingHorizontal: 30,
    borderRadius: 8,
    paddingVertical: 12,
    borderColor: colors.primary,
    color: colors.primary,
    fontSize: 12,
    fontFamily: 'Poppins-Regular',
  },
  tgl: {
    borderColor: colors.primary,
    borderWidth: 1,
    borderRadius: 8,
    paddingVertical: 4,
  },
  containertgl: {
    flexDirection: 'row',
    paddingTop: 0,
    paddingBottom: 15,
    margin: 0,
  },
  deskripsi: {
    fontSize: 14,
    fontFamily: 'Poppins-Regular',
    color: colors.primary,
    flex: 1,
  },
  value: plasholder => ({
    fontSize: 14,
    fontFamily: 'Poppins-Light',
    color: plasholder ? colors.fourth : colors.primary,
  }),
  button: {
    height: 50,
  },
  note: {
    fontFamily: 'Poppins-Light',
    color: colors.primary,
    fontSize: 8,
  },
  conBox: {
    justifyContent: 'center',
    flex: 1,
  },
  input: {
    marginTop: -15,
  },
  textBox: {
    fontFamily: 'Poppins-Light',
    color: colors.primary,
    fontSize: 11,
  },
  pageBox: {
    flexDirection: 'row',
  },
  container: {
    paddingHorizontal: 24,
  },
  page: {
    backgroundColor: colors.tertiary,
    flex: 1,
  },
  alamat: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
});
